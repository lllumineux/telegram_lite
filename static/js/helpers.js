export function formatBytes(bytes) {
    const k = 1024;
    const dm = 1;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    if (bytes <= k) return `${parseFloat((bytes / 1024).toFixed(dm))}KB`;

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

export function getNormalizedExtension(filename) {
    const ext = filename.split('.').pop();
    return ext.length <= 4 ? ext : '';
}

export function isImage(filename) {
    return ['png', 'jpg', 'jpeg'].includes(filename.split('.').pop().toLowerCase())
}

import {
    formatBytes,
    getNormalizedExtension
} from "./helpers.js";

$(document).ready(() => {
    // Auth pages
    const inputSelector = $('input');

    inputSelector.each((i, el) => {
        const labelSelector = $(`label[for="${el.id}"]`);

        el.addEventListener('input', () => {
            el.style.boxShadow = '0 0 0 2px #157ce1'
            labelSelector[0].innerHTML = (el.name.charAt(0).toUpperCase() + el.name.slice(1)).replace('_', ' ');
            labelSelector.css({'color': '#157ce1', 'font-weight': '500'});
        });

        el.addEventListener('mouseenter', () => {
            if (!$(`#${el.id}`).is(':focus') && getComputedStyle(el).boxShadow.split(' ').slice(0, 3).join('') !== 'rgb(223,63,64)') {
                el.style.boxShadow = '0 0 0 1px #3390ec';
                labelSelector.css('color', '#3390ec');
            }

        });

        el.addEventListener('mouseleave', () => {
            if (!$(`#${el.id}`).is(':focus') && getComputedStyle(el).boxShadow.split(' ').slice(0, 3).join('') !== 'rgb(223,63,64)') {
                el.style.boxShadow = '0 0 0 1px #dfe1e5';
                labelSelector.css('color', '#9e9e9e');
            }
        });

        el.addEventListener('focusin', () => {
            if (getComputedStyle(el).boxShadow.split(' ').slice(0, 3).join('') !== 'rgb(223,63,64)') {
                el.style.boxShadow = '0 0 0 2px #157ce1';
                labelSelector.css({'color': '#157ce1', 'font-weight': '500'});
            }
        });

        el.addEventListener('focusout', () => {
            if (getComputedStyle(el).boxShadow.split(' ').slice(0, 3).join('') !== 'rgb(223,63,64)') {
                el.style.boxShadow = '0 0 0 1px #dfe1e5';
                labelSelector.css({'color': '#9e9e9e', 'font-weight': '400'});
            }
        });
    });

    // Posts page

    //Fix Firefox scrollbar problem
    const posts= $('.posts')[0];
    if (posts.scrollHeight > posts.clientHeight) {
        posts.style.paddingLeft = '6px';
        const style = document.createElement('style');
        style.innerHTML = `
            @-moz-document url-prefix() {
                .posts {
                    padding-left: 8px!important;
                }
            }
        `
        $('body')[0].appendChild(style);
    }



    // Other
    const addPostTextareaSelector = $('.add-post-form-inner textarea');
    const addAttachmentInputSelector = $('#id_attachments');
    const addAttachmentButtonSelector = $('label[for="id_attachments"]');
    const addPostButtonSelector = $('.add-post-btn');
    addPostButtonSelector[0].disabled = true;

    const addPostTextInputSelector = $('.add-post-text-input');

    const addAttachmentPopupSelector = $('.add-attachments-popup-overlay');
    const popupTextInput = $('.popup-text-input')[0];

    const addAttachmentSVG = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512"><use href="#add-attachment-icon"><path id="add-attachment-icon" d="M464.059,61.565c-63.814-63.814-167.651-63.814-231.467,0L34.181,259.973c-45.578,45.584-45.575,119.754,0.008,165.335    c22.793,22.793,52.723,34.189,82.665,34.186c29.934-0.003,59.878-11.396,82.668-34.186l181.87-181.872    c27.352-27.346,27.355-71.848,0.003-99.204c-27.352-27.349-71.856-27.348-99.202,0.005L163.258,263.168    c-9.131,9.13-9.131,23.935-0.002,33.067c9.133,9.131,23.935,9.131,33.068,0l118.937-118.934    c9.116-9.117,23.951-9.117,33.067-0.003c9.116,9.119,9.116,23.951-0.003,33.068L166.457,392.241    c-27.352,27.348-71.852,27.352-99.202,0.002c-27.349-27.351-27.352-71.853-0.006-99.204L265.659,94.632    c45.583-45.579,119.752-45.581,165.335,0.002c22.082,22.08,34.244,51.439,34.242,82.666c0,31.228-12.16,60.586-34.245,82.668    L232.587,458.379c-9.131,9.131-9.131,23.935,0.002,33.067c4.566,4.566,10.55,6.848,16.533,6.848    c5.983,0,11.968-2.284,16.534-6.848l198.401-198.409c30.916-30.913,47.941-72.015,47.941-115.735    C512.001,133.58,494.975,92.478,464.059,61.565z"></path></use></svg>';
    addAttachmentButtonSelector[0].innerHTML = addAttachmentSVG;

    addPostTextInputSelector[0].focus();
    addPostTextInputSelector[0].addEventListener('input', () => {
        addPostTextareaSelector[0].value = addPostTextInputSelector[0].innerHTML;
        addPostButtonSelector[0].disabled = addPostTextInputSelector[0].innerHTML === '';

        const height = $('.add-post-form-wrapper')[0].clientHeight;

        $('.posts-wrapper').css('padding-bottom', height);
        const postsSelector = $('.posts');
        postsSelector.css('max-height', `calc(100vh - ${height + 56}px)`);
        postsSelector.scrollTop(postsSelector[0].scrollHeight);
    });

    addPostTextInputSelector[0].addEventListener('keydown', e => {
        if (e.keyCode === 13) {
            e.preventDefault();
            if (addPostTextInputSelector[0].innerHTML !== '') {
                $('.add-post-form')[0].submit();
            }
        }
    });

    addAttachmentInputSelector[0].addEventListener('change', () => {
        addAttachmentButtonSelector[0].innerHTML = addAttachmentSVG;

        const files = [...addAttachmentInputSelector[0].files];
        let filesHTML = '';
        const PopupAttachmentNumberTextSelector = $('.popup-attachment-number-text');
        const PopupAttachmentListSelector = $('.popup-attachment-list');

        PopupAttachmentListSelector[0].innerHTML = '';

        for (const file_index in files) {
            const file = files[file_index];
            filesHTML += `
                <div class="popup-attachment-list-element">
                    <!-- TODO: Add images preview -->
                    <div class="default-attachment-icon">
                        <div class="attachment-extension">${getNormalizedExtension(file.name)}</div>
                    </div>
                    <div class="attachment-info">
                        <div class="attachment-name">${file.name}</div>
                        <div class="attachment-size">${formatBytes(file.size)}</div>
                    </div>
                </div>
            `;
        }
        PopupAttachmentListSelector[0].innerHTML = filesHTML;
        PopupAttachmentNumberTextSelector[0].innerHTML = (addAttachmentInputSelector[0].files.length > 1) ? `Send ${files.length} Files` : 'Send File';
        popupTextInput.innerHTML = addPostTextInputSelector[0].innerHTML;
        addPostTextInputSelector[0].innerHTML = '';

        addAttachmentPopupSelector.css('display', 'flex');
        popupTextInput.focus();

    });

    $('.popup-close-btn')[0].addEventListener('click', () => {
        addPostTextInputSelector[0].innerHTML = popupTextInput.innerHTML;
        popupTextInput.innerHTML = '';
        addPostTextInputSelector[0].focus();
        addAttachmentPopupSelector.css('display', 'none');
    });

    addAttachmentPopupSelector[0].addEventListener('click', e => {
        if (e.target.classList.contains('add-attachments-popup-overlay')) {
            addAttachmentPopupSelector.css('display', 'none');
            addPostTextInputSelector[0].innerHTML = popupTextInput.innerHTML;
            popupTextInput.innerHTML = '';
            addPostTextInputSelector[0].focus();
        }
    });

    popupTextInput.addEventListener('input', () => {
        addPostTextareaSelector[0].value = popupTextInput.innerHTML;
        $('#popup-text-input-placeholder').css(
            'display',
            popupTextInput.innerHTML === '' ? 'inline' : 'none'
        )
    });

    popupTextInput.addEventListener('keydown', e => {
        if (e.keyCode === 13) {
            e.preventDefault();
            $('.add-post-form')[0].submit();
        }
    });

    $('.popup-send-btn')[0].addEventListener('click', () => {
        $('.add-post-form')[0].submit();
    });
});

document.addEventListener("DOMContentLoaded", e => {
    const postsSelector = $('.posts');
    postsSelector.scrollTop(postsSelector[0].scrollHeight);
});
## What is Telegram Lite?
It is a lightweight version of the Telegram Web written in pure Django and JavaScript. Completely copies the design of the web version of Telegram, but has only the functionality for registering accounts, logging in and sending messages to one global chat.

## How to run?

- ### With using Docker
  - Open console in project directory
  - Type `docker-compose up`

- ### Without using Docker
  - Create .env file with the following variables:
    - `POSTGRES_DB`
    - `POSTGRES_USER`
    - `POSTGRES_PASSWORD`
    - `POSTGRES_HOST`
  - Open console in project directory
  - Type `python manage.py migrate`
  - Type `python manage.py runserver`

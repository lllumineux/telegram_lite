from typing import Callable

from django.core.handlers.wsgi import WSGIRequest
from django.shortcuts import redirect

from apps.accounts.models import User


def signup_user(username: str, password: str) -> User:
    user = User(username=username)
    user.set_password(password)
    user.save()
    return user


def check_if_username_taken(username: str) -> bool:
    return User.objects.filter(username=username).exists()


def not_authenticated_required(func: Callable) -> Callable:
    def wrapper(request: WSGIRequest, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('posts')
        return func(request, *args, **kwargs)
    return wrapper


def get_user_amount() -> int:
    return len(User.objects.all())

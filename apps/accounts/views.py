from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect

from apps.accounts.forms import SignupForm, LoginForm
from apps.accounts.services import not_authenticated_required, signup_user


@not_authenticated_required
def signup_view(request):
    context = {'user': {}, 'field_errors': {'username': '', 'password': '', 'confirm_password': ''}}

    if request.method == 'POST':
        form = SignupForm(request.POST)
        context['form'] = form

        if form.is_valid():
            user = signup_user(form.cleaned_data['username'], form.cleaned_data['password'])
            login(request, user)
            return redirect('posts')

        for field, errors in form.errors.items():
            context['field_errors'][field] = errors[0]

    return render(request, 'auth/signup.html', context)


@not_authenticated_required
def login_view(request):
    context = {'user': {}, 'field_errors': {'username': '', 'password': ''}}

    if request.method == 'POST':
        form = LoginForm(request.POST)
        context['form'] = form

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('posts')

            context['field_errors']['username'] = 'Check username and try again'
            context['field_errors']['password'] = 'Check password and try again'

    return render(request, 'auth/login.html', context)


def logout_view(request):
    logout(request)
    return redirect('login')

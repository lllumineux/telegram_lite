import re

from django.contrib.auth.password_validation import (
    MinimumLengthValidator, CommonPasswordValidator,
    NumericPasswordValidator,
)
from django.core.exceptions import ValidationError
from django.utils.translation import ngettext

from apps.accounts.services import check_if_username_taken


def validate_username(username: str) -> None:
    errors = []

    # Check if username 5+ symbols
    if not re.fullmatch(r'[\D|\d]{5,}', username):
        errors.append('Username is less than 5 characters')
    # Check if username matches [a-Z], numbers or underscores
    if not re.fullmatch(r'[\w_]*', username):
        errors.append('Username should only contain letters, numbers, underscores')
    # Check if username is already taken by another user
    if check_if_username_taken(username):
        errors.append('Username already taken')

    if errors:
        raise ValidationError(errors)


class CustomPasswordMinimumLengthValidator(MinimumLengthValidator):
    def validate(self, password, user=None):
        if len(password) < self.min_length:
            raise ValidationError(
                ngettext(
                    f'Password is less than {self.min_length} character',
                    f'Password is less than {self.min_length} characters',
                    self.min_length
                ),
                code='password_too_short',
                params={'min_length': self.min_length},
            )


class CustomCommonPasswordValidator(CommonPasswordValidator):
    def validate(self, password, user=None):
        if password.lower().strip() in self.passwords:
            raise ValidationError('Password is too common', code='password_too_common')


class CustomNumericPasswordValidator(NumericPasswordValidator):
    def validate(self, password, user=None):
        if password.isdigit():
            raise ValidationError('Password is entirely numeric', code='password_entirely_numeric')

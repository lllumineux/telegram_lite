from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.forms import forms, fields, PasswordInput, TextInput

from apps.accounts.validators import validate_username


class CustomForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_suffix = ''


class SignupForm(CustomForm):
    username = fields.CharField(max_length=32, label='Username', widget=TextInput(attrs={'placeholder': 'Username'}))
    password = fields.CharField(max_length=32, label='Password', widget=PasswordInput(attrs={'placeholder': 'Password'}))
    confirm_password = fields.CharField(max_length=32, label='Confirm password', widget=PasswordInput(attrs={'placeholder': 'Confirm password'}))

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()

        if cleaned_data['password'] != cleaned_data['confirm_password']:
            self.add_error('password', 'Password mismatch')
            self.add_error('confirm_password', 'Password mismatch')

        else:
            try:
                validate_password(cleaned_data['password'])
            except ValidationError as e:
                self.add_error('password', list(e)[0])

            try:
                validate_username(cleaned_data['username'])
            except ValidationError as e:
                self.add_error('username', list(e)[0])

        return cleaned_data


class LoginForm(CustomForm):
    username = fields.CharField(max_length=32, label='Username', widget=TextInput(attrs={'placeholder': 'Username'}))
    password = fields.CharField(max_length=32, label='Password', widget=PasswordInput(attrs={'placeholder': 'Password'}))
from django.forms import forms, fields, Textarea, ClearableFileInput


class CustomForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_suffix = ''


class AddPostForm(CustomForm):
    text = fields.CharField(max_length=4096, label='Post text', widget=Textarea(attrs={'placeholder': 'Post', 'cols': '80', 'rows': '1'}), required=False)
    attachments = forms.FileField(label='Post attachments', widget=ClearableFileInput(attrs={'multiple': True}), required=False)

import uuid

from django.db import models
from django.utils import timezone

from apps.accounts.models import User


def upload_location(instance, filename):
    filename = f'{uuid.uuid4()}.{filename.split(".")[-1]}'
    return filename


class Post(models.Model):
    text = models.TextField(max_length=4096, default='', blank=True)
    time_published = models.DateTimeField(default=timezone.now)
    times_viewed = models.IntegerField(default=0)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        ordering = ('time_published',)


class PostAttachment(models.Model):
    url = models.FileField(upload_to=upload_location)
    name = models.CharField(max_length=256)
    size = models.IntegerField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

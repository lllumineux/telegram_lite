import os

from django import template


register = template.Library()


@register.filter
def is_image(f_name: str) -> bool:
    return f_name.split('.')[-1].lower() in ['png', 'jpg', 'jpeg']


@register.filter
def extension(f_name: str) -> str:
    return f_name.split('.')[-1]


@register.filter
def pretty_size(size: int) -> str:
    if size < 1024.0:
        return f'{size/1024:.1f} KB'
    for unit in ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB']:
        if size < 1024.0:
            return f'{size:.1f} {unit}'
        size /= 1024.0
    return f'{size}YB'


@register.filter
def filename(path: str) -> str:
    return os.path.basename(str(path))

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from apps.accounts.services import get_user_amount
from apps.posts.forms import AddPostForm
from apps.posts.services import get_posts, add_post


@login_required(redirect_field_name='')
def posts_view(request):
    context = {
        'form': AddPostForm(),
        'posts': get_posts(),
        'user_amount': get_user_amount()
    }

    if request.method == 'POST':
        form = AddPostForm(request.POST, request.FILES)
        context['form'] = form

        if form.is_valid():
            text = form.cleaned_data['text']
            attachments = request.FILES.getlist('attachments')
            user = request.user

            if text or attachments:
                add_post(text, attachments, user)
                context['form'] = AddPostForm()
                context['posts'] = get_posts()

        return redirect('/')

    return render(request, 'auth/../../templates/posts/posts.html', context)

from django.urls import path

from apps.posts.views import posts_view

urlpatterns = [
    path('', posts_view, name='posts')
]

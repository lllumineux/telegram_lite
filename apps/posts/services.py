from typing import List

from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db.models import QuerySet

from apps.accounts.models import User
from apps.posts.models import Post, PostAttachment


def get_posts() -> QuerySet[Post]:
    posts = Post.objects.all()
    for post in posts:
        post.times_viewed += 1
        post.save()
        post.attachments = PostAttachment.objects.filter(post=post)
    return posts


def add_post(text: str, attachments: List[InMemoryUploadedFile], author: User) -> None:
    post = Post(text=text, author=author)
    post.save()
    for attachment in attachments:
        PostAttachment.objects.create(url=attachment, name=attachment.name, size=attachment.size, post=post)
